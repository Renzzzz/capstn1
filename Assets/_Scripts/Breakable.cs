﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Breakable : Interactable
{
    public float DistractionRadius;
    public float StoppingDistance;
    public float CommotionDuration;
    public List<NPCMovement> Affected = new List<NPCMovement>();
    public override void Interact(GameObject sender)
    {
        base.Interact(sender);
        AttractAttention();
    }

    public virtual IEnumerator AttractAttention()
    {
        List<Collider> affectedObjects = Physics.OverlapSphere(transform.position, DistractionRadius).ToList();
        Affected = affectedObjects.Where(obj => obj.GetComponent<NPCMovement>()).Select(o => o.GetComponent<NPCMovement>()).ToList();
        foreach (NPCMovement npc in Affected)
        {
            npc.target = transform.position;
            npc.State = NPCState.Distracted;
            npc.SetDestination(Vector3.MoveTowards(transform.position, npc.transform.position, StoppingDistance));
            
        }
        yield return new WaitForSeconds(CommotionDuration);
        foreach (NPCMovement npc in Affected)
        {
            npc.State = NPCState.Normal;
            npc.GenerateRandomDestination();
        }
    }

    private void OnMouseDown()
    {
      StartCoroutine(AttractAttention());
    }
    private void OnMouseOver()
    {

    }
}
