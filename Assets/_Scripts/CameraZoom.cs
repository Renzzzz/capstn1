﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{
    public Transform TargetPosition;
    public float TargetSize;
    public float MoveRate;
    public float ZoomRate;
    [HideInInspector]
    public Camera Cam;
    [HideInInspector]
    public Vector3 OriginalPosition;
    [HideInInspector]
    public float OriginalSize;

    IEnumerator zoomIn()
    {
        while (Cam.transform.position.x != TargetPosition.position.x || Cam.transform.position.z != TargetPosition.position.z || Cam.orthographicSize != TargetSize)
        {
            Cam.transform.position = Vector3.MoveTowards(Cam.transform.position, new Vector3(TargetPosition.position.x, Cam.transform.position.y, TargetPosition.position.z), MoveRate);
            Cam.orthographicSize = Mathf.MoveTowards(Cam.orthographicSize, TargetSize, ZoomRate);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator zoomOut()
    {
        while ((Cam.transform.position - OriginalPosition).magnitude >= 0 || Cam.orthographicSize != OriginalSize)
        {
            Cam.transform.position = Vector3.MoveTowards(Cam.transform.position, OriginalPosition, MoveRate);
            Cam.orthographicSize = Mathf.MoveTowards(Cam.orthographicSize, OriginalSize, ZoomRate);
            yield return new WaitForEndOfFrame();
        }
        Destroy(this);
    }

    public void ZoomIn()
    {
        StopAllCoroutines();
        StartCoroutine(zoomIn());
    }

    public void ZoomOut()
    {
        StopAllCoroutines();
        StartCoroutine(zoomOut());
    }
}
