﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomTrigger : MonoBehaviour
{
    public Transform TargetPosition;
    public float TargetSize;
    public float MoveRate;
    public float ZoomRate;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerMove>())
        {
            if (other.gameObject.GetComponent<CameraZoom>())
            {
                CameraZoom cameraZoom = other.gameObject.GetComponent<CameraZoom>();
                cameraZoom.Cam.transform.position = cameraZoom.OriginalPosition;
                cameraZoom.Cam.orthographicSize = cameraZoom.OriginalSize;
                cameraZoom.ZoomIn();
            }
            else
            {
                CameraZoom cameraZoom = other.gameObject.AddComponent<CameraZoom>();
                cameraZoom.TargetPosition = TargetPosition;
                cameraZoom.TargetSize = TargetSize;
                cameraZoom.MoveRate = MoveRate;
                cameraZoom.ZoomRate = ZoomRate;
                cameraZoom.Cam = Camera.main;
                cameraZoom.OriginalPosition = cameraZoom.Cam.transform.position;
                cameraZoom.OriginalSize = cameraZoom.Cam.orthographicSize;
                cameraZoom.ZoomIn();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<CameraZoom>())
            other.gameObject.GetComponent<CameraZoom>().ZoomOut();
    }
}
