﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{
    PlayerMove player;
    Interactable interactableObject;
    // Use this for initialization
    void Start()
    {
        player = GetComponentInParent<PlayerMove>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && interactableObject)      
            interactableObject.GetComponent<Interactable>().Interact(player.gameObject);     
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Interactable>())
            interactableObject = other.gameObject.GetComponent<Interactable>();        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Interactable>())
            interactableObject = null;

    }
}
