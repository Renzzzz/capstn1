﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCMovement : MonoBehaviour
{
    public float MaxDistanceFromSpawn;
    [HideInInspector]
    public NavMeshAgent NavAgent;
    [HideInInspector]
    public NPCState State;
    public Vector3 target;
    Vector3 centralBound;
    float deltaTime;


    void Start()
    {
        NavAgent = GetComponent<NavMeshAgent>();
        State = NPCState.Normal;
        deltaTime = 0;
        centralBound = transform.position;
        GenerateRandomDestination();
    }

    void Update()
    {
        
        if (State.Equals(NPCState.Normal))
        {
            if ((transform.position - NavAgent.destination).magnitude <= 1)
            {
                Debug.Log("Normal");
                NavAgent.Stop();
                deltaTime += Time.deltaTime;
                if (deltaTime >= Random.Range(3, 6))
                {
                    deltaTime = 0;
                    GenerateRandomDestination();
                }
            }
            if ((transform.position - centralBound).magnitude >= MaxDistanceFromSpawn)
                ReturnToArea();
        }
        if (State.Equals(NPCState.Distracted))
        {
            Debug.Log("Distracted");
            transform.LookAt(target);
            this.gameObject.GetComponentInChildren<Transform>().LookAt(target);
        }
        if (State.Equals(NPCState.Chase))
        {
            Debug.Log("chase");
            //transform.LookAt(target);
            SetDestination(target);
        }

    }

    public void GenerateRandomDestination()
    {
        while (true)
        {
            NavAgent.ResetPath();
            NavMeshPath path = new NavMeshPath();
            NavAgent.CalculatePath(new Vector3(transform.position.x + Random.Range(-2, 2), 1, transform.position.z + Random.Range(-2, 2)), path);
            if (!path.status.Equals(NavMeshPathStatus.PathPartial))
            {
                NavAgent.SetPath(path);
                NavAgent.Resume();
                break;
            }
        }
    }

    public void ReturnToArea()
    {
        NavAgent.ResetPath();
        NavAgent.SetDestination(Vector3.MoveTowards(transform.position, centralBound, Random.Range(MaxDistanceFromSpawn / 2, MaxDistanceFromSpawn)));
        NavAgent.Resume();
    }

    public void SetDestination(Vector3 destination)
    {
        NavAgent.ResetPath();
        NavMeshPath path = new NavMeshPath();
        NavAgent.CalculatePath(destination, path);
        NavAgent.SetPath(path);
        NavAgent.Resume();
    }
}

public enum NPCState
{
    Normal,
    Distracted,
    Chase,
    Count
}
