﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetection : MonoBehaviour
{
    public GameObject Spawnpoint;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Hit");
            other.transform.position = new Vector3(Spawnpoint.transform.position.x, other.transform.position.y, Spawnpoint.transform.position.z);
        }
        
    }
}
