﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    Animator animator;
    public float Speed;
    GameObject interactionBox;

    void Start()
    {
        animator = GetComponent<Animator>();
        interactionBox = GetComponentInChildren<Interact>().gameObject;
    }
	void FixedUpdate ()
    {
        Vector3 pos = this.transform.position;

        if (Input.GetKey(KeyCode.W))
        {
            animator.SetBool("isWalking", true);
            animator.SetFloat("Yvalue", 1);
            animator.SetFloat("Xvalue", 0);
            interactionBox.transform.localPosition = new Vector3(0, 0.15f, 0);
            //this.transform.Translate(Vector2.up * speed * Time.deltaTime);
            pos.z += Speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            animator.SetBool("isWalking", true);
            animator.SetFloat("Yvalue", -1);
            animator.SetFloat("Xvalue", 0);
            interactionBox.transform.localPosition = new Vector3(0, -0.15f, 0);
            //this.transform.Translate(Vector2.down * speed * Time.deltaTime);
            pos.z -= Speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            animator.SetBool("isWalking", true);
            animator.SetFloat("Yvalue", 0);
            animator.SetFloat("Xvalue", -1);
            interactionBox.transform.localPosition = new Vector3(-0.15f, 0, 0);
            //this.transform.Translate(Vector2.left * speed * Time.deltaTime);
            pos.x -= Speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            animator.SetBool("isWalking", true);
            animator.SetFloat("Yvalue", 0);
            animator.SetFloat("Xvalue", 1);
            interactionBox.transform.localPosition = new Vector3(0.15f, 0, 0);
            //this.transform.Translate(Vector2.right * speed * Time.deltaTime);
            pos.x += Speed * Time.deltaTime;
        }
        else
        {
            animator.SetBool("isWalking", false);
        }
        transform.position = pos;
    }
}
