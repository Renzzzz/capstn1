﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class InteractQuest : Quest
{
    public bool SpawnObjects; 
    public List<QuestItem> Requirements = new List<QuestItem>();
    //  [HideInInspector]
    public List<GameObject> UncollectedRequirements = new List<GameObject>();

    public override void Interact(GameObject Sender)
    {
        base.Interact(Sender);
        if (base.Progress != null)
            return;
        if (SpawnObjects)
        {
            foreach (QuestItem questItem in Requirements)
            {
                GameObject temp = Instantiate(questItem.Object, questItem.SpawnLocation.position, Quaternion.identity);
                temp.GetComponent<QuestInteract>().Owner = this;
                temp.GetComponent<QuestInteract>().Destructible = questItem.PostDelete;
                temp.transform.eulerAngles = new Vector3(90, 0, 0);
                UncollectedRequirements.Add(temp);
                Destroy(questItem.SpawnLocation.gameObject);
            }
        }
        else
        {
            foreach (QuestItem questItem in Requirements)
            {
                questItem.Object.AddComponent<QuestItemInteract>();
                questItem.Object.GetComponent<QuestInteract>().Owner = this;
                questItem.Object.GetComponent<QuestInteract>().Destructible = questItem.PostDelete;
                UncollectedRequirements.Add(questItem.Object);
                if (questItem.SpawnLocation)
                    Destroy(questItem.SpawnLocation.gameObject);
            }
        }
        base.Progress = StartCoroutine(waitForCompletion());
    }

    IEnumerator waitForCompletion()
    {
        yield return new WaitUntil(() => UncollectedRequirements.Count <= 0);
        base.Finished = true;
    }
}
