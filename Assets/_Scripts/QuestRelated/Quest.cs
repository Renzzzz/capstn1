﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Quest : Interactable
{
    public bool Finished;
    public bool Completed;
    public Coroutine Progress;
    //Difference bet. finished vs completed: taking all items finishes the quest, but talking to the NPC that gave it completes the shit (this is where the rewards kick in)
    public override void Interact(GameObject Sender)
    {
        QuestHandler questHandler = Sender.GetComponent<QuestHandler>();
        if (Finished && !Completed)
        {
            questHandler.Quests.RemoveAt(questHandler.Quests.FindIndex(quest => quest.Equals(this)));
            Completed = true;
            return;
        }
        if (Sender.GetComponent<QuestHandler>().Quests.Exists(quest => quest.Equals(this)) || Completed)
            return;       
        Sender.GetComponent<QuestHandler>().Quests.Add(this);
    }
}
