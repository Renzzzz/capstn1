﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestInteract : Interactable
{
    public Quest Owner;
    public bool Destructible;
    public override void Interact(GameObject sender)
    {
        base.Interact(sender);
        if (Destructible)
            Destroy(gameObject);
        else
        {
            //add new interactable here or sum shit if the objective is a person
            Destroy(this);
        }
    }
}
