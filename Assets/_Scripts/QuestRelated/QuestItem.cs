﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestItem
{
    public string Name;
    public string Description;
    public bool PostDelete;
    public GameObject Object;
    public Transform SpawnLocation;

    void contribute()
    {
        EventManager.TriggerEvent("AddItem");
    }
}
