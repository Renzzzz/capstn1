﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class QuestItemInteract : QuestInteract
{
    public override void Interact(GameObject sender)
    {
        InteractQuest itemQuest = (InteractQuest)Owner;
        itemQuest.UncollectedRequirements.RemoveAt(itemQuest.UncollectedRequirements.FindIndex(questItem => questItem.Equals(gameObject)));
        itemQuest.UncollectedRequirements.TrimExcess();
        base.Interact(sender); 
    }
}
